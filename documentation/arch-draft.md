# Important : mutualiser tout ce qui est mutualisable en amont dans le typescript

## coté ts

* parse le html en gardant les positions
* merge le style avec le default style, le custom style et les highlights
* envois la list des texts et des médias à render au natif avec le style et une key interne
* handle les event natif pour retrouver la position globale des selections

## coté natif

* parse le json
* render les text et les médias avec le style
* catch les events de touch (link(key) ou highlightPress(key)) et de selection ({ start: key, pos, end: key, pos })

```js
onSelect({ start: {id:'aaa', pos:'3'}, end: {id:'vvv', pos:'43'} })

// the graph in ts:
{
    aaa: {
        start: 654,
        end: 654,
        next: 'ccc',
        prev: null
    },
    vvv: { },
}
```
