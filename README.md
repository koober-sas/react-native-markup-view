# react-native-markup-view

**\[DRAFT\]** A react-native view that can display selectable text from a markup language like HTML.

## Installation

```sh
npm install @koober/react-native-markup-view react-native-webview
cd ios && pod install
```

## Usage

```js
import MarkupView from '@koober/react-native-markup-view';

// ...

export default function App() {
  return (
    <MarkupView
      source={{
          encoding: 'text/html',
          content: `<h1>Hello World!</h1>`
        }}
    />
  );
}
```

## License

MIT
