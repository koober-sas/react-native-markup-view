package com.reactnativemarkupview

import android.view.View
import com.facebook.react.uimanager.SimpleViewManager
import com.facebook.react.uimanager.ThemedReactContext
import com.facebook.react.uimanager.annotations.ReactProp
import com.facebook.react.views.text.ReactTextView

class MarkupViewManager : SimpleViewManager<View>() {
  override fun getName() = "MarkupView"

  private var textView: ReactTextView? = null

  @ReactProp(name = "markupItems")
  fun setMarkupJson(view: View, json: String?) {
    textView?.text = parseMarkupItems(json)
  }

  override fun createViewInstance(context: ThemedReactContext): View {
    textView = ReactTextView(context)
    textView?.setTextIsSelectable(true)
    textView?.text = ""
    return textView as ReactTextView
  }
}
