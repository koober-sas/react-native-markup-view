package com.reactnativemarkupview

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.list
import kotlinx.serialization.json.Json

@Serializable
sealed class MarkupItem {
  @Serializable
  @SerialName("text")
  data class Text(
      val type: String = "text",
      val data: String,
      val style: Map<String, String>? = null
  ) : MarkupItem()

  @Serializable
  @SerialName("media")
  data class Media(
      val type: String = "media",
      val subType: String,
      val src: String
  ) : MarkupItem()

  @Serializable
  @SerialName("line")
  data class Line(
      val type: String = "line"
  ) : MarkupItem()
}

fun parseMarkupItems(markupJson: String?): String {
  if (markupJson == null) return ""

  val items = try {
    Json.decodeFromString(ListSerializer(MarkupItem.serializer()), markupJson)
  } catch (e: Exception) {
    listOf<MarkupItem>()
  }
  return getItemsData(items).joinToString("")
}

fun getItemsData(items: List<MarkupItem>): List<String> {
  return items.fold(mutableListOf<String>()) { result, item ->
    when (item) {
      is MarkupItem.Text -> result.add(item.data)
      else -> {}
    }
    result
  }
}
