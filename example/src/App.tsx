import * as React from 'react';
import { StyleSheet, View, SafeAreaView, FlatList, Dimensions } from 'react-native';
import { MarkupView, Highlight, Selection } from '@koober/react-native-markup-view';
import { Font, Button, ButtonGroup, Select, ToggleButton } from './ui';
import MarkupPage from './MarkupPage';
import { snippets } from './snippets';

export default function App() {
  const [selection, setSelection] = React.useState<undefined | Selection>(undefined);
  const [counter, setCounter] = React.useState(0);
  const [fontSize, setFontSize] = React.useState(MarkupView.defaultFontSize);
  const [currentIndex, setCurrentIndex] = React.useState(0);
  const [currentFlatListIndex, setCurrentFlatListIndex] = React.useState(0);
  const [selectable, setSelectable] = React.useState(true);
  const [scrollEnabled, setScrollEnabled] = React.useState(true);
  const [parentCollapsible, setParentCollapsible] = React.useState(false);
  const [fontFamily, setFontFamily] = React.useState(Font.Times);
  const [highlightColor, setHighlightColor] = React.useState<undefined | string>(undefined);
  const [highlightsById, setHighlightsById] = React.useState<{ [key: string]: Highlight[] }>({});
  const [currentHighlight, setCurrentHighlight] = React.useState<Highlight | undefined>(undefined);
  const addHighlight = (id: string, highlight: Highlight) => {
    const oldHighlights = id in highlightsById ? highlightsById[id] : [];
    setHighlightsById({ ...highlightsById, [id]: [...oldHighlights, { id: String(Math.random()), ...highlight }] });
  };
  const removeHighlightById = (id: string, highlightId: Highlight['id']) => {
    setHighlightsById({
      ...highlightsById,
      [id]: highlightsById[id].filter((highlight) => highlight.id !== highlightId),
    });
  };
  const computedScrollEnabled = parentCollapsible ? false : scrollEnabled;
  const exampleList = () =>
    snippets.map(({ snippetName }, index) => (
      <Button
        key={`example-btn-${index}`}
        onPress={() => setCurrentIndex(index)}
        style={styles.exampleBtn}
        title={snippetName}
      />
    ));

  const { snippetContent } = snippets[currentIndex];

  const handleCounterButtonClick = () => {
    setCounter(counter + 1);
  };

  const handleFontSizeButtonClick = (step: number) => {
    setFontSize(fontSize + step * 2);
  };

  const markupPageProps = {
    counter,
    fontFamily,
    fontSize,
    highlightColor,
    parentCollapsible,
    scrollEnabled: computedScrollEnabled,
    selectable,
    selection,
    setSelection,
    setCurrentHighlight,
  };

  const handleScrollEnd = (e: any) => {
    const contentOffset = e.nativeEvent.contentOffset;
    const viewSize = e.nativeEvent.layoutMeasurement;

    // Divide the horizontal offset by the width of the view to see which page is visible
    const pageNum = Math.floor(contentOffset.x / viewSize.width);
    setCurrentFlatListIndex(pageNum);
  };

  return (
    <SafeAreaView style={{ flexDirection: 'column', flex: 1 }}>
      <ButtonGroup>{exampleList()}</ButtonGroup>
      {Array.isArray(snippetContent) ? (
        <FlatList
          bounces={false}
          data={snippetContent}
          directionalLockEnabled
          extraData={{ scrollEnabled, selectable, counter, fontSize }}
          horizontal
          initialNumToRender={1}
          initialScrollIndex={0}
          keyExtractor={(_, index: number) => String(index)}
          onMomentumScrollEnd={handleScrollEnd}
          pagingEnabled
          removeClippedSubviews
          renderItem={(data) => (
            <View style={{ width: Dimensions.get('screen').width }}>
              <MarkupPage
                source={data.item}
                {...markupPageProps}
                highlights={highlightsById[`${currentIndex}_${currentFlatListIndex}`]}
                parentCollapsible={false}
              />
            </View>
          )}
          scrollsToTop={false}
        />
      ) : (
          <MarkupPage
            highlights={highlightsById[`${currentIndex}_${currentFlatListIndex}`]}
            source={snippetContent}
            {...markupPageProps}
          />
        )}

      <ButtonGroup>
        <Button
          onPress={() => {
            if (selection) {
              addHighlight(`${currentIndex}_${currentFlatListIndex}`, selection);
            }
          }}
          title={'✎+'}
        />
        <Button
          disabled={!currentHighlight}
          onPress={() => {
            if (currentHighlight) {
              removeHighlightById(`${currentIndex}_${currentFlatListIndex}`, currentHighlight?.id);
              setCurrentHighlight(undefined);
            }
          }}
          title={'✎-'}
        />
        <Select
          choices={[
            { label: Font.Times, value: Font.Times },
            { label: `${Font.Lato}(remote)`, value: Font.Lato },
            { label: `${Font.Oswald}(local)`, value: Font.Oswald },
          ]}
          onValueChange={setFontFamily}
          value={fontFamily}
        />
        <Select
          choices={[
            { label: 'default', value: undefined },
            { label: 'Red', value: '#FF0000' },
            { label: 'Yellow', value: '#FFFF00' },
            { label: 'Green', value: '#00FF00' },
          ]}
          onValueChange={setHighlightColor}
          value={highlightColor}
        />
        <ToggleButton onValueChange={setSelectable} title={'selectable'} value={selectable} />
        <ToggleButton onValueChange={setParentCollapsible} title={'parent collapsible'} value={parentCollapsible} />
        <ToggleButton
          disabled={parentCollapsible}
          onValueChange={setScrollEnabled}
          title={'scroll'}
          value={computedScrollEnabled}
        />
        <Button onPress={() => handleCounterButtonClick()} title={'N+1'} />
        <Button onPress={() => handleFontSizeButtonClick(1)} title={'  +  '} />
        <Button onPress={() => handleFontSizeButtonClick(-1)} title={'  -  '} />
      </ButtonGroup>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  exampleBtn: {
    margin: 2,
  },
});
