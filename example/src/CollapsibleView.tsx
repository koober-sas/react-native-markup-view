import * as React from 'react';
import { Animated, Easing, View } from 'react-native';

const RATIO_RANGE = [0, 1];

enum CollapseAlign {
  Start = 'start',
  Center = 'center',
  End = 'end',
}

enum CollapseDirection {
  Horizontal = 'horizontal',
  Vertical = 'vertical',
}

type RootElementProps = React.ComponentProps<typeof View>;

type CollapsibleViewProps = RootElementProps & {
  animationDuration: number;
  animationEasing: (input: number) => number;

  /**
   * Is the view collapsed or expanded
   */
  collapsed: boolean;

  /**
   * Minimum size when collapsed
   */
  collapsedMin: number;

  /**
   * Collapse direction
   */
  collapseDirection: 'horizontal' | 'vertical';

  /**
   * Collapse anchor respectively to direction
   */
  collapseAlign: 'start' | 'center' | 'end';

  /**
   * Callback when collapse animation has ended
   */
  onAnimationEnd: () => unknown;

  /**
   * Component children
   */
  children?: React.ReactNode;
};

type CollapsibleViewState = {
  collapseRatio: Animated.Value;
  clientHeight: number;
  clientWidth: number;
  animating: boolean;
};

export class CollapsibleView extends React.Component<CollapsibleViewProps, CollapsibleViewState> {
  static defaultProps = {
    animationDuration: 300,
    animationEasing: Easing.out(Easing.cubic),
    collapseAlign: CollapseAlign.Start,
    collapseDirection: CollapseDirection.Vertical,
    collapsed: true,
    collapsedMin: 0,
    onAnimationEnd: () => null,
  };

  unmounted = false;
  animation: any | null | undefined;

  state = {
    collapseRatio: new Animated.Value(this.props.collapsed ? 0 : 1),
    clientHeight: 0,
    clientWidth: 0,
    animating: false,
  };

  componentDidUpdate(previousProps: CollapsibleViewProps): void {
    const previousCollased = previousProps.collapsed;
    const nextCollapsed = this.props.collapsed;
    if (previousCollased !== nextCollapsed) {
      this.collapsedDidChange(previousCollased, nextCollapsed);
    }
  }

  componentWillUnmount(): void {
    this.unmounted = true;
  }

  handleAnimationEnd = (): void => {
    if (!this.unmounted) {
      this.setState({ animating: false }, () => {
        if (!this.unmounted) {
          this.props.onAnimationEnd();
        }
      });
    }
  };

  handleContentSizeChange = (contentWidth: number, contentHeight: number): void => {
    this.setState({
      clientWidth: contentWidth,
      clientHeight: contentHeight,
    });
  };

  collapsedDidChange(previousCollased: boolean, nextCollapsed: boolean): void {
    this.animateRatio(nextCollapsed ? 0 : 1);
  }

  animateRatio(collapseRatio: number) {
    const { animationDuration, animationEasing } = this.props;

    if (this.animation) {
      this.animation.stop();
    }
    this.setState({ animating: true });
    this.animation = Animated.timing(this.state.collapseRatio, {
      toValue: collapseRatio,
      duration: animationDuration,
      easing: animationEasing,
      useNativeDriver: false,
    }).start(this.handleAnimationEnd);
  }

  render(): React.ReactNode {
    const { collapsed, collapsedMin, collapseDirection, children, style, pointerEvents, ...otherProps } = this.props;
    const { animating, collapseRatio, clientHeight, clientWidth } = this.state;
    const isHorizontal = collapseDirection === CollapseDirection.Horizontal;
    const animatedStyle =
      animating || collapsed
        ? isHorizontal
          ? {
            // overflow: 'hidden',
            width: collapseRatio.interpolate({
              inputRange: RATIO_RANGE,
              outputRange: [collapsedMin, clientWidth],
            }),
          }
          : {
            // overflow: 'hidden',
            height: collapseRatio.interpolate({
              inputRange: RATIO_RANGE,
              outputRange: [collapsedMin, clientHeight],
            }),
          }
        : undefined;
    const contentStyle = isHorizontal
      ? {
        width: clientWidth,
      }
      : {
        height: clientHeight,
      };

    return (
      <Animated.ScrollView
        horizontal={isHorizontal}
        onContentSizeChange={this.handleContentSizeChange}
        pointerEvents={collapsed ? 'none' : pointerEvents ?? 'auto'}
        scrollEnabled={false}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={[style, contentStyle, animatedStyle]}
        {...otherProps}
      >
        {children}
      </Animated.ScrollView>
    );
  }
}
