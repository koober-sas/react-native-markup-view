import equal from 'fast-deep-equal';
import { HTMLNode, HTMLTextNode, HTMLTagNode, HTMLDocument } from './parser';
import { encodeHTMLEntities } from './htmlEntities';

export function getNodePredictableId(node: HTMLNode, childIndex = 0, parentId = ''): string {
  return `${parentId}:${HTMLNode.isTextNode(node) ? `T${encodeURI(node.data)}` : `N${node.name}`}#${childIndex}`;
}

export type NodeId = string;

export interface VirtualBaseNode {
  parentId?: NodeId;
  childIndex: number;
  parentsCount: number;
}

export type VirtualTextNode = VirtualBaseNode & Pick<HTMLTextNode, 'type' | 'data'>;

export type VirtualTagNode = VirtualBaseNode & Pick<HTMLTagNode, 'type' | 'name' | 'attribs'>;

export type VirtualDOMNode = VirtualTextNode | VirtualTagNode;

export const VirtualDOMNode = Object.freeze({
  isTagNode: (node: VirtualDOMNode): node is VirtualTagNode => node.type === 'tag',
});

export type VirtualDOM = { [id: string]: VirtualDOMNode };

export type NodeDiff = {
  attribs?: VirtualTagNode['attribs'];
  data?: VirtualTextNode['data'];
  parentId?: VirtualDOMNode['parentId'];
  name?: VirtualTagNode['name'];
  childIndex?: VirtualDOMNode['childIndex'];
};

export interface NodeInsert {
  op: 'insert';
  id: NodeId;
  node: NodeDiff;
}

export interface NodeDelete {
  op: 'delete';
  id: NodeId;
}

export interface DeleteEverything {
  op: 'delete_everything';
}

export interface NodeUpdate {
  op: 'update';
  id: NodeId;
  node: NodeDiff;
}

export type NodeDifference = NodeInsert | NodeDelete | DeleteEverything | NodeUpdate;

export function getDifferences(previousAST: VirtualDOM, newAST: VirtualDOM): NodeDifference[] {
  const currentNodeIds = Object.keys(previousAST);
  const newNodeIds = Object.keys(newAST);

  const toDelete: NodeDelete[] | DeleteEverything[] = currentNodeIds.some((id) => newNodeIds.includes(id))
    ? currentNodeIds.filter((id) => !newNodeIds.includes(id)).map((id) => ({ op: 'delete', id }))
    : [{ op: 'delete_everything' }];

  const toInsert: NodeInsert[] = newNodeIds
    .filter((id) => !currentNodeIds.includes(id))
    .sort((idA, idB) => {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const nodeA = newAST[idA]!;
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const nodeB = newAST[idB]!;
      if (nodeA.parentsCount === nodeB.parentsCount) {
        return nodeA.childIndex - nodeB.childIndex;
      }

      return nodeA.parentsCount - nodeB.parentsCount;
    })
    .map((id) => ({
      op: 'insert',
      id,
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      node: transformNodeForDiff(newAST[id]!),
    }));

  const toUpdate: NodeUpdate[] = currentNodeIds
    .filter((id) => {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const previousNode = previousAST[id]!;
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const newNode = newAST[id]!;

      return (
        newNodeIds.includes(id) &&
        VirtualDOMNode.isTagNode(previousNode) &&
        VirtualDOMNode.isTagNode(newNode) &&
        !equal(previousNode.attribs, newNode.attribs)
      );
    })
    .map((id) => ({
      op: 'update',
      id,
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      node: transformNodeForDiff(newAST[id]!),
    }));

  return (toDelete as NodeDifference[]).concat(toInsert).concat(toUpdate);
}

function transformNodeForDiff({ type, parentsCount, ...node }: VirtualDOMNode): NodeDiff {
  return node;
}

export const emptyAST: VirtualDOM = Object.freeze({});

export function combineNodes(dom: HTMLDocument): VirtualDOM {
  type ReducerT = {
    markupNodes: VirtualDOM;
    parentId: VirtualDOMNode['parentId'];
    parentsCount: VirtualDOMNode['parentsCount'];
  };

  const nodeReducer = (
    { markupNodes, parentId, parentsCount }: ReducerT,
    node: HTMLNode,
    nodeIndex: number
  ): ReducerT => {
    const id = getNodePredictableId(node, nodeIndex, parentId);

    switch (node.type) {
      case 'text': {
        markupNodes[id] = {
          type: 'text',
          // TODO: remove encodeURI : necessity is not proven, but removing breaks plainText parser
          data: encodeURI(encodeHTMLEntities(node.data)),
          parentId,
          childIndex: nodeIndex,
          parentsCount,
        };
        break;
      }
      case 'tag': {
        markupNodes[id] = {
          type: 'tag',
          name: node.name,
          childIndex: nodeIndex,
          parentId,
          parentsCount,
          attribs: node.attribs,
        };

        node.children.reduce(nodeReducer, {
          markupNodes,
          parentId: id,
          parentsCount: parentsCount + 1,
        });
        break;
      }
      default:
        break;
    }

    return { markupNodes, parentId, parentsCount };
  };

  const { markupNodes: nodes } = dom.reduce(nodeReducer, {
    markupNodes: {},
    parentId: undefined,
    parentsCount: 0,
  });

  return nodes;
}
