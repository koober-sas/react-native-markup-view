/* eslint-disable @typescript-eslint/explicit-function-return-type, no-console */
import { createActionReceiver, createActionSender } from './action';

describe('createActionReceiver()', () => {
  test('should do nothing for any invalid value', async () => {
    jest.spyOn(console, 'warn').mockImplementation(() => undefined);
    const receiver = createActionReceiver();
    await expect(receiver(undefined)).resolves.toBe(undefined);
    expect(console.warn).toHaveBeenLastCalledWith('[MarkupView]', 'undefined is not a valid event');

    await expect(receiver(null)).resolves.toBe(undefined);
    expect(console.warn).toHaveBeenLastCalledWith('[MarkupView]', 'null is not a valid event');

    await expect(receiver({})).resolves.toBe(undefined);
    expect(console.warn).toHaveBeenLastCalledWith('[MarkupView]', '[object Object] is not a valid event');

    await expect(receiver({ type: '' })).resolves.toBe(undefined);
  });

  describe('{type: "openURL", url}', () => {
    test('should open url when {type: "openURL", url}', async () => {
      const Linking = {
        canOpenURL: jest.fn().mockReturnValue(true),
        openURL: jest.fn().mockResolvedValue(undefined),
      };
      const receiver = createActionReceiver({
        Linking,
      });
      await receiver({ type: 'openURL', url: 'my/url' });
      expect(Linking.openURL).toHaveBeenCalledWith('my/url');
    });
    test('should do nothing when {type: "openURL", url} and Linking.canOpenURL returned false', async () => {
      const Linking = {
        canOpenURL: jest.fn().mockReturnValue(false),
        openURL: jest.fn().mockResolvedValue(undefined),
      };
      const receiver = createActionReceiver({
        Linking,
      });
      await receiver({ type: 'openURL', url: 'my/url' });
      expect(Linking.openURL).not.toHaveBeenCalled();
    });
  });
  describe('{type: "ready"}', () => {
    test('should call onReady', async () => {
      const onReady = jest.fn();
      const receiver = createActionReceiver({
        onReady,
      });
      await receiver({ type: 'ready' });
      expect(onReady).toHaveBeenCalled();
    });
  });
  describe('{type: "resize"}', () => {
    test('should call onResize', async () => {
      const onResize = jest.fn();
      const receiver = createActionReceiver({
        onResize,
      });
      await receiver({ type: 'resize', documentHeight: 100 });
      expect(onResize).toHaveBeenCalledWith({ height: 100 });
    });
  });
});
describe('createActionSender()', () => {
  test('should do nothing for an undefined webview', () => {
    const sender = createActionSender({
      globalHandlerName: 'onTestMessage',
      getWebView: () => undefined,
    });
    expect(sender({ type: '' })).toBe(undefined);
  });
  test('should call injectJavascript on webview', () => {
    const webView = {
      injectJavaScript: jest.fn(),
    };
    const sender = createActionSender({
      globalHandlerName: 'onTestMessage',
      getWebView: () => webView,
    });
    expect(sender({ type: 'toto', blah: true })).toBe(undefined);
    expect(webView.injectJavaScript).toHaveBeenLastCalledWith(`onTestMessage('{"type":"toto","blah":true}');true;`);
  });
  test('should encode correctly single quoted string', () => {
    const sender = createActionSender({
      globalHandlerName: 'onTestMessage',
      getWebView: () => undefined,
    });
    expect(sender.message({ type: 'toto', encoded: "'toto'" })).toEqual(
      `onTestMessage('{"type":"toto","encoded":"\\'toto\\'"}');true;`
    );
  });
  test('should encode correctly double quoted string', () => {
    const sender = createActionSender({
      globalHandlerName: 'onTestMessage',
      getWebView: () => undefined,
    });
    expect(sender.message({ type: 'toto', encoded: '"toto"' })).toEqual(
      `onTestMessage('{"type":"toto","encoded":"\\"toto\\""}');true;`
    );
  });
});
