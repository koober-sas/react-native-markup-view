import * as React from 'react';
import { View } from 'react-native';
import { WebView as RNCWebView, WebViewMessageEvent } from 'react-native-webview';
import { Parser, ParserOptions, parseHTML, parsePlainText, HTMLDocument } from './parser';
import { getDifferences, emptyAST, combineNodes, VirtualDOM } from './virtual-dom';
import { StyleSheet } from './styleSheet';
import { defaultStyleSheet } from './defaultStyleSheet';
import webViewHTML from './webView';
import {
  FontFace,
  Selection,
  Highlight,
  ContextMenuRequest,
  ContextMenuItem,
  SelectionChangeEvent,
  HighlightPressEvent,
} from './native';
import {
  StyleUpdateAction,
  DOMUpdateAction,
  WebViewSelectionAction,
  createActionReceiver,
  createActionSender,
} from './action';
import log from './log';
import { combineHighlights } from './highlight';

type WebViewProps = React.ComponentPropsWithRef<typeof RNCWebView>;
type WebViewComponent = React.ComponentType<WebViewProps>;

type Maybe<T> = T | null | undefined;

// eslint-disable-next-line no-shadow
export enum StyleSheetId {
  Default = '__style_default',
  Custom = '__style_custom',
  UserSelection = '__style_selection',
  Font = '__style_font',
  UserHighlight = '__style_highlight',
}

function isNone(anyValue: unknown): anyValue is null | undefined {
  return anyValue === null || anyValue === undefined;
}

type ContentEncoding = 'text/html' | 'text/plain';

export type ContentURISource = {
  /**
   * URI to download content (not supported)
   */
  uri: string;
};
export type ContentTextSource = {
  /**
   * Content encoding (html,...)
   */
  encoding: ContentEncoding;
  /**
   * Full content as string
   */
  content: string;
};
export type ContentSource = ContentTextSource | ContentURISource;

export function isContentSourceEqual(left: ContentSource, right: ContentSource): boolean {
  const leftObject = left as Record<string, unknown>;
  const rightObject = right as Record<string, unknown>;

  return (
    leftObject['uri'] === rightObject['uri'] &&
    leftObject['encoding'] === rightObject['encoding'] &&
    leftObject['content'] === rightObject['content']
  );
}

export function isContentTextSource(anyValue: unknown): anyValue is ContentTextSource {
  return typeof anyValue === 'object' && anyValue !== null && 'content' in anyValue;
}

export function isContentURISource(anyValue: unknown): anyValue is ContentURISource {
  return typeof anyValue === 'object' && anyValue !== null && 'uri' in anyValue;
}

type ViewProps = React.ComponentProps<typeof View>;

export type MarkupViewProps<H extends Highlight> = ParserOptions & {
  /**
   * A style object that allow you to customize the WebView container style.
   * Please note that there are default styles (example: you need to add flex: 0 to the style if you want to use height property).
   */
  containerStyle?: ViewProps['style'];
  /**
   * An array of font face declaration
   */
  fonts?: ReadonlyArray<FontFace>;
  /**
   * Array of text ranges that should be highlighted with an optional id
   */
  highlights?: ReadonlyArray<H>;
  /**
   * Highlight color
   */
  highlightColor?: string;
  /**
   * Called when the user taps the highlight
   */
  onHighlightPress?: (event: HighlightPressEvent<H>) => void;
  /**
   * Callback that is called just after the webview is ready to render
   */
  onReady?: () => void;
  /**
   * Callback that is called when the text selection is changed.
   * This will be called with `{ event: { selection: { start, end } } }`
   */
  onSelectionChange?: (event: SelectionChangeEvent) => void;
  /**
   * [NOT_IMPLEMENTED] A callback that returns an array of menu item.
   * If the callback returns null or undefined then no menu is shown
   */
  requestContextMenu?: (request: ContextMenuRequest<H>) => Maybe<ReadonlyArray<ContextMenuItem>>;
  /**
   * Enable scroll inside web view
   */
  scrollEnabled?: boolean;
  /**
   * Lets the user select text, to use the native copy and paste functionality. default = true.
   */
  selectable?: boolean;
  /**
   * [NOT_IMPLEMENTED] The start and end of the text input's selection. Set start and end to the same value to position the cursor.
   */
  selection?: Pick<Selection, 'start' | 'end'>;
  /**
   * [NOT_IMPLEMENTED] The current highlighted color of the text.
   */
  selectionColor?: string;
  /**
   * Boolean value that determines whether a horizontal scroll indicator is
   * shown in the `MarkupView`. The default value is `true`.
   */
  showsHorizontalScrollIndicator?: boolean;
  /**
   * Boolean value that determines whether a vertical scroll indicator is
   * shown in the `MarkupView`. The default value is `true`.
   */
  showsVerticalScrollIndicator?: boolean;
  /**
   * The content source (either a remote URL or a text content).
   *
   * The currently supported formats are html.
   */
  source?: Maybe<ContentSource>;
  /**
   * The component style
   */
  style?: ViewProps['style'];
  /**
   * Provide your styles for specific HTML tags.
   */
  styleSheet?: StyleSheet;
  /**
   * WebView component type (default to react-community/WebView)
   */
  WebViewComponent?: Maybe<WebViewComponent>;
};

type MarkupViewState = {
  contentRaw: Maybe<ContentTextSource>;
  document: HTMLDocument;
  documentHighlighted: HTMLDocument;
  highlights: MarkupViewProps<Highlight>['highlights'];
  virtualDOM: VirtualDOM;
  contentParser: Parser;
  styleSheetMap: Record<Exclude<StyleSheetId, StyleSheetId.Default>, string>;
  webViewSize: { height: number };
};

export class MarkupView<H extends Highlight = Highlight> extends React.PureComponent<
  MarkupViewProps<H>,
  MarkupViewState
> {
  /**
   * Default parser (plain text)
   */
  static defaultParser = parsePlainText;

  /**
   * Default font size used to generate stylesheet
   */
  static defaultFontSize = 16;

  /**
   * Dictionary of parser by encoding
   */
  static parser = {
    'text/html': parseHTML,
    'text/plain': MarkupView.defaultParser,
  } as const;

  protected static defaultWebViewProps = {
    allowsBackForwardNavigationGesture: false,
    allowsLinkPreview: true,
    automaticallyAdjustContentInset: false,
    bounce: false,
    decelerationRate: 'fast' as const,
    javaScriptEnabled: true,
    originWhitelist: ['*'],
    pagingEnabled: false,
    scrollEnabled: true,
    scalesPageToFit: false,
    source: { html: webViewHTML },
  };

  protected static defaultStyleSheetMap: MarkupViewState['styleSheetMap'] = {
    [StyleSheetId.Custom]: '',
    [StyleSheetId.UserSelection]: '',
    [StyleSheetId.Font]: '',
    [StyleSheetId.UserHighlight]: '',
  };

  static getDerivedStateFromProps<T extends Highlight = Highlight>(
    props: MarkupViewProps<T>,
    state: MarkupViewState
  ): null | Partial<MarkupViewState> {
    const changes: Partial<MarkupViewState> = {};

    // Check stylesheet changes
    const style = MarkupView.getDerivedStyleFromProps(props);
    if ((Object.keys(style) as Array<keyof typeof style>).some((key) => style[key] !== state.styleSheetMap[key])) {
      changes.styleSheetMap = style;
    }

    if (state.document !== state.documentHighlighted || props.highlights !== state.highlights) {
      changes.documentHighlighted = state.document;
      changes.highlights = props.highlights;
      changes.virtualDOM = combineNodes(combineHighlights(state.document, props.highlights ?? []));
    }

    return changes;
  }

  static getDerivedStyleFromProps<T extends Highlight = Highlight>(
    props: MarkupViewProps<T>
  ): MarkupViewState['styleSheetMap'] {
    return {
      [StyleSheetId.Custom]: props.styleSheet != null ? StyleSheet.stringify(props.styleSheet) : '',
      [StyleSheetId.UserSelection]: StyleSheet.stringify({
        body: {
          userSelect: props.selectable === true ? 'auto' : 'none',
          WebkitUserSelect: props.selectable === true ? 'auto' : 'none',
        },
      }),
      [StyleSheetId.Font]: (props.fonts ?? [])
        .map((fontFace) =>
          StyleSheet.stringify({
            '@font-face': fontFace,
          })
        )
        .join(' '),
      [StyleSheetId.UserHighlight]:
        props.highlightColor != null ? StyleSheet.stringify({ mark: { backgroundColor: props.highlightColor } }) : '',
    };
  }

  override state: MarkupViewState = {
    contentRaw: undefined,
    document: HTMLDocument.empty,
    documentHighlighted: HTMLDocument.empty,
    virtualDOM: emptyAST,
    highlights: undefined,
    contentParser: MarkupView.defaultParser,
    styleSheetMap: MarkupView.defaultStyleSheetMap,
    webViewSize: { height: 100 },
  };

  webView = React.createRef<RNCWebView>();

  isReady = false;

  onAction = createActionReceiver({
    onReady: () => this.webviewDidMount(),
    onResize: (webViewSize) => this.setState({ webViewSize }),
    onSelectionChange: (selection) => this.handleSelectionChange(selection),
    onHighlightPress: (highlightId) => {
      const { onHighlightPress, highlights } = this.props;
      const currentHighlight = highlights?.find((highlight) => highlight.id === highlightId);
      if (onHighlightPress != null && currentHighlight != null) {
        onHighlightPress({ highlight: currentHighlight });
      }
    },
  });

  postAction = createActionSender({
    globalHandlerName: 'onWebViewMessage',
    getWebView: () => this.webView.current ?? undefined,
  });

  override componentDidMount(): void {
    this.loadContent();
  }

  override componentDidUpdate(previousProps: MarkupViewProps<H>, previousState: MarkupViewState): void {
    if (this.shouldLoadContent(previousProps, previousState)) {
      this.loadContent();
    }
    if (this.shouldParseContent(previousProps, previousState)) {
      this.parseContent();
    }

    this.styleSheetMapDidChange(previousState.styleSheetMap, this.state.styleSheetMap);

    if (this.isReady && previousState.virtualDOM !== this.state.virtualDOM) {
      this.virtualDOMDidChange(previousState.virtualDOM, this.state.virtualDOM);
    }
  }

  protected getWebViewComponent(): WebViewComponent {
    return this.props.WebViewComponent ?? RNCWebView;
  }

  protected getParser(encoding: ContentEncoding): Maybe<Parser> {
    return MarkupView.parser[encoding];
  }

  protected setContent(contentRaw: Maybe<ContentTextSource>): void {
    const contentParser = contentRaw != null ? this.getParser(contentRaw.encoding) : MarkupView.defaultParser;
    if (__DEV__ && contentParser == null) {
      log('warn', `${contentRaw?.encoding ?? ''} is not supported. Content will be displayed as plain text.`);
    }

    this.setState({
      contentRaw,
      contentParser: contentParser ?? MarkupView.defaultParser,
    });
  }

  protected setDocument(document: HTMLDocument): void {
    this.setState({
      document,
    });
  }

  protected handleWebViewMessage = (event: WebViewMessageEvent): Promise<void> => {
    return this.onAction(JSON.parse(event.nativeEvent.data));
  };

  protected handleWebViewShouldStartLoad = (request: {
    url?: string;
    title?: string;
    loading?: boolean;
    canGoBack?: boolean;
    canGoForward?: boolean;
  }): boolean => {
    const { url } = request;

    if (url === 'about:blank') {
      return true;
    }

    if (url != null) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      this.onAction({ type: 'openURL', url });
    }

    return false;
  };

  protected handleSelectionChange = (selection: WebViewSelectionAction['selection']): void => {
    const { onSelectionChange } = this.props;
    if (onSelectionChange != null) {
      onSelectionChange({
        selection:
          selection != null
            ? { text: selection.text, start: selection.start, end: selection.end }
            : { text: '', start: 0, end: 0 },
      });
    }
  };

  shouldLoadContent(previousProps: MarkupViewProps<H>, previousState: MarkupViewState): boolean {
    const previousSource = previousProps.source;
    const nextSource = this.props.source;

    if (isNone(previousSource)) {
      return !isNone(nextSource);
    }
    if (isNone(nextSource)) {
      return true;
    }

    return !isContentSourceEqual(previousSource, nextSource);
  }

  loadContent(): void {
    const { source } = this.props;
    if (isContentURISource(source)) {
      throw new Error('NotImplementedError'); // TODO: implement
    } else if (isContentTextSource(source)) {
      this.setContent(source);
    } else {
      this.setContent(undefined);
    }
  }

  shouldParseContent(previousProps: MarkupViewProps<H>, previousState: MarkupViewState): boolean {
    const previousContent = previousState.contentRaw;
    const nextContent = this.state.contentRaw;

    if (isNone(previousContent)) {
      return !isNone(nextContent);
    }
    if (isNone(nextContent)) {
      return true;
    }

    return !isContentSourceEqual(previousContent, nextContent);
  }

  parseContent(): void {
    const { contentRaw, contentParser } = this.state;
    if (isNone(contentRaw)) {
      this.setDocument(HTMLDocument.empty);
    } else {
      const { normalizeWhitespace } = this.props;
      const contentParsed = contentParser(contentRaw.content, { normalizeWhitespace });

      this.setDocument(contentParsed.ok ? contentParsed.result : HTMLDocument.empty);
    }
  }

  webviewDidMount(): void {
    const { onReady } = this.props;
    // Set default style
    this.postAction(
      StyleUpdateAction(
        StyleSheetId.Default,
        StyleSheet.stringify(defaultStyleSheet(`${MarkupView.defaultFontSize}px`))
      )
    );
    this.styleSheetMapDidChange(MarkupView.defaultStyleSheetMap, this.state.styleSheetMap);
    this.virtualDOMDidChange({}, this.state.virtualDOM);
    this.isReady = true;
    if (onReady != null) {
      onReady();
    }
  }

  virtualDOMDidChange(previousAST: VirtualDOM, newAST: VirtualDOM): void {
    const diffs = getDifferences(previousAST, newAST);
    if (diffs.length > 0) {
      this.postAction(DOMUpdateAction(diffs));
    }
  }

  styleSheetMapDidChange(
    previousStyleSheetMap: MarkupViewState['styleSheetMap'],
    nextStyleSheetMap: MarkupViewState['styleSheetMap']
  ): void {
    (Object.keys(nextStyleSheetMap) as Array<keyof MarkupViewState['styleSheetMap']>).forEach((styleSheetId) => {
      const previousStyleSheet = previousStyleSheetMap[styleSheetId];
      const nextStyleSheet = nextStyleSheetMap[styleSheetId];
      if (previousStyleSheet !== nextStyleSheet) {
        this.postAction(StyleUpdateAction(styleSheetId, nextStyleSheet));
      }
    });
  }

  override render(): React.ReactNode {
    const { scrollEnabled, containerStyle, style, showsHorizontalScrollIndicator, showsVerticalScrollIndicator } =
      this.props;
    const WebView = this.getWebViewComponent();

    return (
      <WebView
        ref={this.webView}
        onMessage={this.handleWebViewMessage}
        onShouldStartLoadWithRequest={this.handleWebViewShouldStartLoad}
        {...MarkupView.defaultWebViewProps}
        containerStyle={[{ height: this.state.webViewSize.height }, containerStyle]}
        scrollEnabled={scrollEnabled}
        showsHorizontalScrollIndicator={showsHorizontalScrollIndicator}
        showsVerticalScrollIndicator={showsVerticalScrollIndicator}
        style={style}
      />
    );
  }
}
