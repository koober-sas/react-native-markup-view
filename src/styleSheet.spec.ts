import { StyleSheet } from './styleSheet';

describe('StyleSheet', () => {
  describe('.stringify()', () => {
    test('should return empty string for empty stylesheet', () => {
      expect(StyleSheet.stringify({})).toEqual('');
    });
    test('should return a valid sheet for one rule', () => {
      expect(
        StyleSheet.stringify({
          html: {
            fontSize: '1px',
          },
        })
      ).toEqual('html {font-size:1px}');
    });
    test('should return a valid sheet for one rule and many properties', () => {
      expect(
        StyleSheet.stringify({
          html: {
            fontSize: '1px',
            fontFamily: 'Test',
          },
        })
      ).toEqual('html {font-size:1px;font-family:Test}');
    });
    test('should return a valid sheet for many rule', () => {
      expect(
        StyleSheet.stringify({
          html: {
            fontSize: '1px',
          },
          body: {
            fontSize: '2px',
          },
          p: {
            fontSize: '3px',
          },
        })
      ).toEqual('html {font-size:1px}body {font-size:2px}p {font-size:3px}');
    });

    test('should handle undefined values', () => {
      expect(
        StyleSheet.stringify({
          html: {
            fontSize: '1px',
            fontFamily: undefined,
          },
        })
      ).toEqual('html {font-size:1px}');
    });
  });
});
