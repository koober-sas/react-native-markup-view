import { encode, decode } from 'he';

const defaultOptions = { useNamedReferences: true };

export function encodeHTMLEntities(value: string): string {
  return encode(value, defaultOptions);
}

export function decodeHTMLEntities(value: string): string {
  return decode(value);
}
