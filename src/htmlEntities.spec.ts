import { decodeHTMLEntities, encodeHTMLEntities } from './htmlEntities';

describe('decodeHTMLEntities', () => {
  test('should decode properly an encoded', () => {
    expect(decodeHTMLEntities('foo&nbsp;bar')).toEqual('foo\u00A0bar');
  });
});
describe('encodeHTMLEntities', () => {
  test('should decode properly an encoded', () => {
    expect(encodeHTMLEntities('foo\u00A0bar')).toEqual('foo&nbsp;bar');
  });
});
