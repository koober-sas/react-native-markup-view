import { Linking as RNLinking } from 'react-native';
import type { NodeDifference } from './virtual-dom';
import log from './log';

// eslint-disable-next-line @typescript-eslint/ban-types
export type Action<T extends string, P extends Record<string, unknown> = {}> = Readonly<
  {
    type: T;
  } & P
>;

/**
 * Send a DOM update using the diff
 */
export type DOMUpdateAction = Action<
  'DOMUpdate',
  {
    diff: NodeDifference[];
  }
>;
export function DOMUpdateAction(diff: NodeDifference[]): DOMUpdateAction {
  return {
    type: 'DOMUpdate',
    diff,
  };
}
/**
 * Send a global style update
 */
export type StyleUpdateAction = Action<
  'StyleUpdate',
  {
    styleElementId: string;
    css: string;
  }
>;
export function StyleUpdateAction(styleElementId: string, css: string): StyleUpdateAction {
  return {
    type: 'StyleUpdate',
    styleElementId,
    css,
  };
}

/**
 * Any action sent to the WebView component
 */
export type WebViewInternalInAction = DOMUpdateAction | StyleUpdateAction;

/**
 * Sent once after the initialization of the main script
 */
export type WebViewReadyAction = Action<'ready'>;

/**
 * Sent after a DOM or Style update
 */
export type WebViewResizeAction = Action<'resize', { documentHeight: number; documentWidth: number }>;

/**
 * Sent after a content selection
 */
export type WebViewSelectionAction = Action<
  'selection',
  {
    selection:
      | undefined
      | {
          text: string;
          /**
           * Start character position from document.body
           */
          start: number;
          /**
           * End character position from document.body
           */
          end: number;
        };
  }
>;

export type WebViewHighlightPressAction = Action<
  'highlightPress',
  {
    id: string;
  }
>;

/**
 * Request the opening of an url
 */
export type ConsoleAction = Action<
  'console',
  {
    level: 'debug' | 'warn' | 'log' | 'error' | 'info';
    arguments: unknown[];
  }
>;

/**
 * Request the opening of an url
 */
export type OpenURLAction = Action<
  'openURL',
  {
    url: string;
  }
>;

/**
 * Any action sent from the WebView component
 */
export type WebViewInternalOutAction =
  | WebViewReadyAction
  | WebViewResizeAction
  | WebViewSelectionAction
  | WebViewHighlightPressAction
  | OpenURLAction
  | ConsoleAction;

export type ActionReceiver = (data: unknown) => Promise<void>;

export function createActionReceiver({
  Linking = RNLinking,
  onReady = () => undefined,
  onResize = () => undefined,
  onSelectionChange = () => undefined,
  onHighlightPress = () => undefined,
}: {
  Linking?: Pick<typeof RNLinking, 'openURL' | 'canOpenURL'>;
  onReady?: () => void;
  onResize?: (viewport: { height: number }) => void;
  onSelectionChange?: (selection: WebViewSelectionAction['selection']) => void;
  onHighlightPress?: (highlightId: string) => void;
} = {}): ActionReceiver {
  function isValidAction(anyValue: unknown): anyValue is WebViewInternalOutAction {
    // @ts-ignore type guard difficult to type inside
    return anyValue != null && typeof anyValue.type === 'string';
  }

  return async (data) => {
    if (!isValidAction(data)) {
      log('warn', `${String(data)} is not a valid event`);

      return;
    }

    switch (data.type) {
      case 'console': {
        log(data.level, ...data.arguments, '(from WebView)');
        break;
      }
      case 'ready': {
        onReady();
        break;
      }
      case 'resize': {
        onResize({ height: data.documentHeight });
        break;
      }
      case 'highlightPress': {
        onHighlightPress(data.id);
        break;
      }
      case 'selection': {
        onSelectionChange(data.selection);
        break;
      }
      case 'openURL': {
        const { url } = data;
        if (await Linking.canOpenURL(url)) {
          await Linking.openURL(url);
        }
        break;
      }
      default:
        if (__DEV__) {
          // eslint-disable-next-line @typescript-eslint/restrict-template-expressions, @typescript-eslint/no-unsafe-member-access
          log('warn', `${(data as any).type} is not a supported event`);
        }
    }
  };
}

export type ActionSender = {
  (event: { type: string } & Record<string, unknown>): void;
  message(event: { type: string } & Record<string, unknown>): string;
};

export function createActionSender({
  globalHandlerName,
  getWebView,
}: {
  globalHandlerName: string;
  getWebView: () => undefined | { injectJavaScript(script: string): void };
}): ActionSender {
  function escapeCharacters(content: string, escapedCharacters: string): string {
    return String(content).replace(new RegExp(`\\\\([\\S\\s])|([${escapedCharacters}])`, 'g'), '\\$1$2');
  }
  function sender(event: { type: string } & Record<string, unknown>): void {
    const webView = getWebView();
    if (webView != null) {
      webView.injectJavaScript(sender.message(event));
    }
  }
  sender.message = (event: { type: string } & Record<string, unknown>): string => {
    return `${globalHandlerName}('${escapeCharacters(JSON.stringify(event), `'`)}');true;`;
  };

  return sender;
}
