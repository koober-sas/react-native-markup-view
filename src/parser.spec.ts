import { HTMLDocument, parseHTML, parsePlainText, HTMLNode } from './parser';

describe('parsePlainText()', () => {
  test('should return a single text node', () => {
    expect(parsePlainText('foobar')).toEqual({
      ok: true,
      result: [
        {
          type: 'text',
          data: 'foobar',
          textStartPosition: 0,
          textEndPosition: 6,
        },
      ],
    });
  });
  test('should handle utf8 character', () => {
    expect(parsePlainText('foo\u00A0bar')).toEqual({
      ok: true,
      result: [
        {
          type: 'text',
          data: 'foo\u00A0bar',
          textStartPosition: 0,
          textEndPosition: 7,
        },
      ],
    });
  });
});

describe('parseHTML()', () => {
  test('should return a tree', () => {
    expect(parseHTML('<h1><b>foo</b>bar<b>baz</b></h1>')).toEqual({
      ok: true,
      result: [
        {
          type: 'tag',
          name: 'h1',
          attribs: {},
          children: [
            {
              type: 'tag',
              name: 'b',
              attribs: {},
              children: [
                {
                  type: 'text',
                  data: 'foo',
                  textStartPosition: 0,
                  textEndPosition: 3,
                },
              ],
            },
            {
              type: 'text',
              data: 'bar',
              textStartPosition: 3,
              textEndPosition: 6,
            },
            {
              type: 'tag',
              name: 'b',
              attribs: {},
              children: [
                {
                  type: 'text',
                  data: 'baz',
                  textStartPosition: 6,
                  textEndPosition: 9,
                },
              ],
            },
          ],
        },
      ],
    });
  });

  test('should parse html entities', () => {
    expect(parseHTML('<h1>foo&nbsp;bar</h1>')).toEqual({
      ok: true,
      result: [
        HTMLNode.createTagNode({
          name: 'h1',
          children: [
            HTMLNode.createTextNode({
              data: 'foo\u00A0bar',
              textStartPosition: 0,
              textEndPosition: 7,
            }),
          ],
        }),
      ],
    });
  });
});

describe('HTMLDocument', () => {
  const fromHTML = (html: string): HTMLDocument => {
    const parsed = parseHTML(html);
    if (parsed.ok) {
      return parsed.result;
    }
    throw new Error('Malformed HTML error');
  };

  describe('flatMap()', () => {
    test('should return unchanged when identity', () => {
      expect(
        HTMLDocument.flatMap(fromHTML('<h1><b>foo</b> bar <b>baz</b></h1>'), (textNode) => [textNode])
      ).toMatchObject(fromHTML('<h1><b>foo</b> bar <b>baz</b></h1>'));
    });
    test('should return a changed tree', () => {
      expect(
        HTMLDocument.flatMap(fromHTML('<h1><b>foo</b> bar <b>baz</b></h1>'), (textNode) =>
          textNode.data === 'foo'
            ? [
                HTMLNode.createTagNode({
                  name: 'mark',
                  children: [textNode],
                  attribs: {},
                }),
              ]
            : [textNode]
        )
      ).toMatchObject(fromHTML('<h1><b><mark>foo</mark></b> bar <b>baz</b></h1>'));
    });
    test('should return be able to split text', () => {
      expect(
        HTMLDocument.flatMap(fromHTML('<h1><b>foo</b> bar <b>baz</b></h1>'), (textNode) =>
          textNode.data === 'foo'
            ? [
                HTMLNode.createTextNode({
                  data: 'fo',
                  textStartPosition: 0,
                  textEndPosition: 2,
                }),
                HTMLNode.createTagNode({
                  name: 'mark',
                  children: [
                    HTMLNode.createTextNode({
                      data: 'o',
                      textStartPosition: 2,
                      textEndPosition: 3,
                    }),
                  ],
                  attribs: {},
                }),
              ]
            : [textNode]
        )
      ).toMatchObject(fromHTML('<h1><b>fo<mark>o</mark></b> bar <b>baz</b></h1>'));
    });
  });
});
