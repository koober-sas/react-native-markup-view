/**
 * Font face declaration
 */
export type FontFace = {
  fontFamily: string | undefined;
  fontWeight?:
    | 'normal'
    | 'bold'
    | 'lighter'
    | 'bolder'
    | '100'
    | '200'
    | '300'
    | '400'
    | '500'
    | '600'
    | '700'
    | '800'
    | '900'
    | number
    | undefined;
  fontStyle?: 'normal' | 'italic' | 'oblique' | undefined;
  src: string;
  unicodeRange?: string | undefined;
};

export type Selection = {
  /**
   * The start character index of the selected range
   */
  start: number;
  /**
   * The end character index of the selected range
   */
  end: number;
  /**
   * the selected text
   */
  text: string;
};

export type SelectionChangeEvent = {
  /**
   * The selection range
   */
  selection: Selection;
};

export type HighlightPressEvent<H extends Highlight = Highlight> = {
  /**
   * The pressed highlight
   */
  highlight: H;
};

export type Highlight = {
  /**
   * Optional id to refer to this highlight
   */
  id?: string | number;
} & Selection;

export const Highlight = Object.freeze({
  create: (props: Partial<Highlight>): Highlight => ({
    start: 0,
    end: 0,
    text: '',
    ...props,
  }),
});

export type ContextMenuRequestHighlight<H extends Highlight> = {
  type: 'highlight';
  target: H;
};

export type ContextMenuRequestSelection = {
  type: 'selection';
  target: Selection;
};

export type ContextMenuRequest<H extends Highlight> = ContextMenuRequestHighlight<H> | ContextMenuRequestSelection;

export type ContextMenuItem = {
  /**
   * Custom identifier for context menu item
   */
  id: number | string;
  /**
   * The label displayed in the context menu
   */
  label: string;
  /**
   * Callback when the menu item is pressed
   */
  onPress?: () => void;
};

export type SelectionPosition = {
  /**
   * The node id in the AST
   */
  nodeId: string;
  /**
   * Offset in the node element
   */
  offset: number;
};
