/* eslint-disable @typescript-eslint/explicit-function-return-type, no-console */
import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { WebView as RNCWebView } from 'react-native-webview';
import { MarkupView, isContentSourceEqual, isContentTextSource, isContentURISource, StyleSheetId } from './MarkupView';
import { Highlight } from './native';

jest.mock('react-native', () => ({
  View: () => 'View',
  StyleSheet: jest.requireActual('react-native').StyleSheet,
}));
jest.mock('react-native-webview', () => ({
  WebView: () => 'WebView',
}));

describe('.isContentSourceEqual()', () => {
  test('should return true when shallow equality is true', () => {
    expect(
      isContentSourceEqual({ encoding: 'text/html', content: 'text' }, { encoding: 'text/html', content: 'text' })
    ).toBe(true);
  });
  test('should return false when encoding is different', () => {
    expect(
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      isContentSourceEqual({ encoding: 'text/html', content: 'text' }, { encoding: 'html2' as any, content: 'text' })
    ).toBe(false);
  });
  test('should return false when text is different', () => {
    expect(
      isContentSourceEqual({ encoding: 'text/html', content: 'text' }, { encoding: 'text/html', content: 'other text' })
    ).toBe(false);
  });
  test('should return false when uri is different', () => {
    expect(isContentSourceEqual({ uri: 'my/uri' }, { uri: 'my/other/uri' })).toBe(false);
    expect(isContentSourceEqual({ uri: 'my/uri' }, { encoding: 'text/html', content: 'my content' })).toBe(false);
  });
});
describe('isContextTextSource()', () => {
  test('should return true for ContextTextSource', () => {
    expect(isContentTextSource({ encoding: 'text/html', content: 'content' })).toBe(true);
  });
  test('should return true for other values', () => {
    expect(isContentTextSource(null)).toBe(false);
    expect(isContentTextSource(undefined)).toBe(false);
    expect(isContentTextSource({})).toBe(false);
  });
});
describe('isContextURISource()', () => {
  test('should return true for ContextURISource', () => {
    expect(isContentURISource({ uri: 'my/uri' })).toBe(true);
  });
  test('should return true for other values', () => {
    expect(isContentURISource(null)).toBe(false);
    expect(isContentURISource(undefined)).toBe(false);
    expect(isContentURISource({})).toBe(false);
  });
});

describe(`<${MarkupView.name}>`, () => {
  const createElement = (props: Partial<React.ComponentProps<typeof MarkupView>> = {}) =>
    shallow<MarkupView<Highlight>, React.ComponentProps<typeof MarkupView>>(
      React.createElement(MarkupView, {
        ...props,
      })
    );

  test('should render correctly', () => {
    expect(createElement().exists()).toBe(true);
  });
  const findWebView = (wrapper: ShallowWrapper) => wrapper.find(RNCWebView);
  const findCustomWebView = (wrapper: ShallowWrapper, customWebView: typeof RNCWebView) => wrapper.find(customWebView);
  const findRootView = (wrapper: ShallowWrapper) => wrapper;

  const emptyState: MarkupView['state'] = {
    contentRaw: undefined,
    document: [],
    documentHighlighted: [],
    highlights: [],
    virtualDOM: {},
    contentParser: MarkupView.defaultParser,
    styleSheetMap: {
      [StyleSheetId.Custom]: '',
      [StyleSheetId.UserSelection]: '',
      [StyleSheetId.Font]: '',
      [StyleSheetId.UserHighlight]: '',
    },
    webViewSize: { height: 0 },
  };
  const emptyProps = {};

  describe('.getDerivedStyleFromProps()', () => {
    test('should return empty object', () => {
      expect(MarkupView.getDerivedStyleFromProps({})).toEqual({
        [StyleSheetId.Custom]: '',
        [StyleSheetId.UserSelection]: 'body {user-select:none;-webkit-user-select:none}',
        [StyleSheetId.Font]: '',
        [StyleSheetId.UserHighlight]: '',
      });
    });
    test('should return a user selection when selectable is true', () => {
      expect(MarkupView.getDerivedStyleFromProps({ selectable: true })).toEqual(
        expect.objectContaining({
          [StyleSheetId.UserSelection]: 'body {user-select:auto;-webkit-user-select:auto}',
        })
      );
    });
    test('should return a new stylesheet for fonts', () => {
      expect(
        MarkupView.getDerivedStyleFromProps({
          fonts: [
            {
              fontFamily: 'Lato',
              src: 'url(my/url)',
            },
          ],
        })
      ).toEqual(
        expect.objectContaining({
          [StyleSheetId.Font]: '@font-face {font-family:Lato;src:url(my/url)}',
        })
      );
    });
    test('should return a user highlight when highlightColor is set', () => {
      expect(MarkupView.getDerivedStyleFromProps({ highlightColor: 'yellow' })).toEqual(
        expect.objectContaining({
          [StyleSheetId.UserHighlight]: 'mark {background-color:yellow}',
        })
      );
    });
  });

  describe('#componentDidUpdate()', () => {
    test('should trigger a loadContent when shouldLoadContent is true', () => {
      const instance = createElement().instance();
      jest.spyOn(instance, 'loadContent');
      jest.spyOn(instance, 'shouldLoadContent').mockReturnValue(true);

      instance.componentDidUpdate(instance.props, instance.state);

      expect(instance.loadContent).toHaveBeenCalled();
    });
    test('should not trigger a loadContent when shouldLoadContent is false', () => {
      const instance = createElement().instance();
      jest.spyOn(instance, 'loadContent');
      jest.spyOn(instance, 'shouldLoadContent').mockReturnValue(false);

      instance.componentDidUpdate(instance.props, instance.state);

      expect(instance.loadContent).not.toHaveBeenCalled();
    });
    test('should trigger a parseContent() when shouldParseContent() is true', () => {
      const instance = createElement().instance();
      jest.spyOn(instance, 'parseContent');
      jest.spyOn(instance, 'shouldParseContent').mockReturnValue(true);

      instance.componentDidUpdate(instance.props, instance.state);

      expect(instance.parseContent).toHaveBeenCalled();
    });
    test('should not trigger a parseContent() when shouldParseContent() is false', () => {
      const instance = createElement().instance();
      jest.spyOn(instance, 'parseContent');
      jest.spyOn(instance, 'shouldLoadContent').mockReturnValue(false);

      instance.componentDidUpdate(instance.props, instance.state);

      expect(instance.parseContent).not.toHaveBeenCalled();
    });
  });

  describe('#shouldLoadContent()', () => {
    test('should return false when source has not changed and is null', () => {
      expect(
        createElement({
          source: undefined,
        })
          .instance()
          .shouldLoadContent({ source: undefined }, emptyState)
      ).toBe(false);
    });
    test('should return false when source has not changed and is set', () => {
      expect(
        createElement({
          source: { encoding: 'text/html', content: 'content' },
        })
          .instance()
          .shouldLoadContent({ source: { encoding: 'text/html', content: 'content' } }, emptyState)
      ).toBe(false);
    });
    test('should return true when source changed', () => {
      expect(
        createElement({
          source: { encoding: 'text/html', content: 'old content' },
        })
          .instance()
          .shouldLoadContent({ source: { encoding: 'text/html', content: 'new content' } }, emptyState)
      ).toBe(true);

      expect(
        createElement({
          source: { encoding: 'text/html', content: 'old content' },
        })
          .instance()
          .shouldLoadContent({ source: undefined }, emptyState)
      ).toBe(true);

      expect(
        createElement({
          source: undefined,
        })
          .instance()
          .shouldLoadContent({ source: { encoding: 'text/html', content: 'old content' } }, emptyState)
      ).toBe(true);
    });
  });

  describe('#shouldParseContent()', () => {
    test('should return false when contentRaw has not changed and is null', () => {
      expect(
        createElement()
          .setState({
            contentRaw: null,
          })
          .instance()
          .shouldParseContent(emptyProps, {
            ...emptyState,
            contentRaw: null,
          })
      ).toBe(false);
    });
    test('should return false when source has not changed and is set', () => {
      expect(
        createElement()
          .setState({
            contentRaw: { encoding: 'text/html', content: 'content' },
          })
          .instance()
          .shouldParseContent(emptyProps, { ...emptyState, contentRaw: { encoding: 'text/html', content: 'content' } })
      ).toBe(false);
    });
    test('should return true when source changed', () => {
      expect(
        createElement()
          .setState({
            contentRaw: { encoding: 'text/html', content: 'old content' },
          })
          .instance()
          .shouldParseContent(emptyProps, {
            ...emptyState,
            contentRaw: { encoding: 'text/html', content: 'new content' },
          })
      ).toBe(true);

      expect(
        createElement()
          .setState({
            contentRaw: { encoding: 'text/html', content: 'old content' },
          })
          .instance()
          .shouldParseContent(emptyProps, { ...emptyState, contentRaw: null })
      ).toBe(true);

      expect(
        createElement({
          source: undefined,
        })
          .instance()
          .shouldParseContent(emptyProps, {
            ...emptyState,
            contentRaw: { encoding: 'text/html', content: 'old content' },
          })
      ).toBe(true);
    });
  });

  describe('#props.WebViewComponent', () => {
    test('should use react-native-webview.WebView by default', () => {
      const wrapper = createElement();
      expect(findWebView(wrapper).type()).toBe(RNCWebView);
    });
    test('should use WebViewComponent value if set', () => {
      const CustomWebView = RNCWebView;
      const wrapper = createElement({
        WebViewComponent: RNCWebView,
      });

      expect(findCustomWebView(wrapper, CustomWebView).type()).toBe(CustomWebView);
    });
  });
  describe('#props.scrollEnabled', () => {
    test('should forward to WebView', () => {
      expect(findWebView(createElement({ scrollEnabled: true })).prop('scrollEnabled')).toBe(true);
      expect(findWebView(createElement({ scrollEnabled: false })).prop('scrollEnabled')).toBe(false);
    });
  });
  describe('#props.showsHorizontalScrollIndicator', () => {
    test('should forward to WebView', () => {
      expect(
        findWebView(createElement({ showsHorizontalScrollIndicator: true })).prop('showsHorizontalScrollIndicator')
      ).toBe(true);
      expect(
        findWebView(createElement({ showsHorizontalScrollIndicator: false })).prop('showsHorizontalScrollIndicator')
      ).toBe(false);
    });
  });
  describe('#props.showsVerticalScrollIndicator', () => {
    test('should forward to WebView', () => {
      expect(
        findWebView(createElement({ showsVerticalScrollIndicator: true })).prop('showsVerticalScrollIndicator')
      ).toBe(true);
      expect(
        findWebView(createElement({ showsVerticalScrollIndicator: false })).prop('showsVerticalScrollIndicator')
      ).toBe(false);
    });
  });
  describe('#props.containerStyle', () => {
    test('should forward to webview', () => {
      const customStyle = {};
      expect(findWebView(createElement({ containerStyle: customStyle })).prop('containerStyle')).toEqual([
        expect.any(Object),
        customStyle,
      ]);
    });
  });
  describe('#props.style', () => {
    test('should forward to root', () => {
      const customStyle = { flex: 1 };
      expect(findRootView(createElement({ style: customStyle })).prop('style')).toEqual(customStyle);
    });
  });

  describe('#webviewDidMount()', () => {
    test('should call props.onReady()', () => {
      const onReady = jest.fn();
      const element = createElement({ onReady });

      element.instance().webviewDidMount();
      expect(onReady).toHaveBeenCalledTimes(1);
    });
  });
});
