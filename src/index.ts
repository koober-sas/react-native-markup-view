/* eslint-disable filenames/match-exported */
import { MarkupView, MarkupViewProps, ContentURISource, ContentTextSource, ContentSource } from './MarkupView';

export { MarkupView, MarkupViewProps, ContentURISource, ContentTextSource, ContentSource };
export * from './native';
export default MarkupView;
