import { StyleSheet, TagStyle } from './styleSheet';

// @see https://github.com/necolas/normalize.css/blob/master/normalize.css
// @see https://gist.github.com/matthewhudson/3055535#file-uiwebview-css

export function defaultStyleSheet(baseFontSize: number | string): StyleSheet {
  const listStyle = {
    display: 'block',
    margin: `${em(1)} 0`,
    padding: 0,
    paddingInlineStart: '40px', // for LTL/RTL
  };
  const subSupStyle = {
    fontSize: percent(75),
    lineHeight: 0,
    position: 'relative',
    verticalAlign: 'baseline',
  };

  return {
    '*': {
      /* Safari displays a callout containing information when you touch and hold a touch target such as a link. This property allows you to disable that callout. */
      WebkitTouchCallout: 'none',

      /* Specifies a size adjustment for displaying text content in Safari on iPhone. */
      WebkitTextSizeAdjust: 'none',
    },
    // block tabs
    html: {
      fontSize: baseFontSize,
    },
    body: {
      fontSize: rem(1),
      margin: 0,
      padding: 0,
    },
    div: {},
    ul: {
      ...listStyle,
    },
    ol: {
      ...listStyle,
    },
    iframe: {
      height: rem(12.5),
    },
    hr: {
      boxSizing: 'content-box',
      height: 0,
      overflow: 'visible',
    },
    // inline tabs
    u: { textDecorationLine: 'underline' },
    em: { fontStyle: 'italic' },
    i: { fontStyle: 'italic' },
    b: { fontWeight: 'bolder' },
    s: { textDecorationLine: 'line-through' },
    strong: { fontWeight: 'bolder' },
    big: { fontSize: percent(120) },
    small: { fontSize: percent(80) },
    a: {
      textDecorationLine: 'underline',
      color: '#245dc1',
    },
    h1: headingStyle(2, 0.67),
    h2: headingStyle(1.5, 0.83),
    h3: headingStyle(1.17, 1),
    h4: headingStyle(1, 1.33),
    h5: headingStyle(0.83, 1.67),
    h6: headingStyle(0.67, 2.33),
    sub: {
      ...subSupStyle,
      bottom: em(-0.25),
    },
    sup: {
      ...subSupStyle,
      top: em(-0.5),
    },
    p: {
      margin: `${em(1)} 0`,
    },
  };
}

/**
 * Small utility for generating heading styles
 *
 * @param fontMultiplier the amount to multiply the font size by
 * @param marginMultiplier the amount to multiply the margin by
 * @returns a style def for a heading
 */
function headingStyle(fontMultiplier = 1, marginMultiplier = 1): TagStyle {
  return {
    fontSize: rem(fontMultiplier),
    margin: `${em(marginMultiplier)} 0`,
    fontWeight: 'bold',
  };
}

function decimal(value: number): string {
  return Number(value)
    .toFixed(2)
    .replace(/[,.]00$/, '');
}

function rem(value: number): string {
  return `${decimal(value)}rem`;
}

function em(value: number): string {
  return `${decimal(value)}em`;
}

function percent(value: number): string {
  return `${decimal(value)}%`;
}
