export default `
  <!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
  <script>
(function () {
  function __createContext(main) {
    const require = function require(id) {
      throw new Error('NotImplemented');
    };
    require.main = main;
    const module = { exports: {} };
    return [require, module, module.exports];
  }

  (function (require, module, exports) {
  /**
 * @typedef {import('../src/virtual-dom').NodeDiff} NodeDiff
 * @typedef {import('../src/action').WebViewInternalInAction} WebViewInternalInAction
 * @typedef {import('../src/action').WebViewInternalOutAction} WebViewInternalOutAction
 */

/** @typedef {Readonly<{
  viewport: {
    width: number,
    height: number
  }
}>} AppState */
const AppState = (function () {
  /** @type {AppState} */
  let state = {
    viewport: {
      width: 0,
      height: 0,
    },
  };

  /**
   * @returns AppState
   */
  function get() {
    return state;
  }

  /** @type {(change: Partial<AppState>|undefined) => void} */
  function update(change) {
    if (change && Object.keys(change).length > 0) {
      // @ts-ignore
      state = assign(assign({}, state), change);
    }
  }

  return {
    get,
    update,
  };
})();
exports.AppState = AppState;

/**
 * DOM manipulation/selection module
 */
const DOM = (function () {
  /**
   * Return true if \`anyValue\` is an Element
   *
   * @param {any} anyValue
   * @returns {anyValue is Element}
   */
  function isElement(anyValue) {
    return anyValue && typeof anyValue.tagName === 'string';
  }

  /**
   * Return an array of all element ancestors
   *
   * @param {Element} element
   * @returns {Element[]}
   */
  function getElementAncestors(element) {
    const returnValue = [];
    let currentElement = element;
    while (currentElement.parentElement) {
      currentElement = currentElement.parentElement;
      returnValue.push(currentElement);
    }

    return returnValue;
  }

  /**
   * Remove every children of element
   *
   * @param {Element} element
   */
  function removeChildren(element) {
    while (element.firstChild) {
      element.firstChild.remove();
    }
  }

  /**
   * Merge all \`attributeMap\` in element attributes
   *
   * @param {HTMLElement} element
   * @param {{style?: string | HTMLElement['style']} & Record<string, unknown>} attributeMap
   */
  function setAttributes(element, attributeMap) {
    // https://github.com/jquery/jquery/blob/master/src/attributes/attr.js
    const nodeType = element.nodeType;
    // Don't get/set attributes on text, comment and attribute nodes
    if (nodeType === 3 || nodeType === 8 || nodeType === 2) {
      return;
    }

    const attributeNames = Object.keys(attributeMap);

    // eslint-disable-next-line unicorn/no-for-loop
    for (let attributeIndex = 0; attributeIndex < attributeNames.length; attributeIndex += 1) {
      /** @type {string} */
      // @ts-ignore
      const attributeName = attributeNames[attributeIndex];
      switch (attributeName) {
        case 'class':
          element.className = String(attributeMap[attributeName]);
          break;
        case 'style': {
          const attributeValue = attributeMap[attributeName];
          if (typeof attributeValue === 'string') {
            element.style.cssText = attributeValue;
          } else if (attributeValue) {
            merge(element.style, attributeValue);
          }
          break;
        }
        default: {
          const attributeValue = attributeMap[attributeName];
          if (attributeValue == null) {
            element.removeAttribute(attributeName);
          } else {
            element.setAttribute(attributeName, String(attributeValue));
          }
        }
      }
    }
  }

  return {
    isElement,
    getElementAncestors,
    removeChildren,
    setAttributes,
  };
})();
exports.DOM = DOM;

const TextPosition = (function () {
  // @see https://github.com/tilgovi/dom-anchor-text-position/
  /** @type {(r: Range)=> string} */
  const stringifyRange = String;

  /**
   *
   * @param {Element} root
   * @param {Range} range
   * @returns {{start: number, end: number}}
   */
  function fromRange(root, range) {
    const document = root.ownerDocument;
    const prefix = document.createRange();

    const startNode = range.startContainer;
    const startOffset = range.startOffset;

    prefix.setStart(root, 0);
    prefix.setEnd(startNode, startOffset);

    const start = stringifyRange(prefix).length;
    const end = start + stringifyRange(range).length;

    return {
      start,
      end,
    };
  }

  return {
    fromRange,
  };
})();
exports.TextPosition = TextPosition;

function consoleService() {
  /**
   *
   * @param {'debug'|'warn'|'log'|'error'|'info'} level
   */
  function forwardFunction(level) {
    const originalFunction = console[level];

    return function () {
      // eslint-disable-next-line prefer-rest-params
      const argumentArray = Array.prototype.slice.call(arguments);
      exports.postAction({
        type: 'console',
        level,
        arguments: argumentArray,
      });
      // @ts-ignore
      originalFunction.apply(this, argumentArray);
    };
  }

  console.debug = forwardFunction('debug');
  console.warn = forwardFunction('warn');
  console.error = forwardFunction('error');
  console.log = forwardFunction('log');
  console.info = forwardFunction('info');
}

function actionService() {
  // @ts-ignore
  assign(window, { onWebViewMessage: exports.handleWebViewMessage });
}

function displayService() {
  const meta = document.createElement('meta');
  meta.setAttribute('content', 'initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0');
  meta.setAttribute('name', 'viewport');
  document.head.append(meta);
}

function highlightService() {
  /**
   *
   * @param {any} element
   * @returns {element is HTMLElement}
   */
  function isMarkElement(element) {
    return DOM.isElement(element) && element.tagName.toLowerCase() === 'mark';
  }

  /**
   *
   * @param {HTMLElement} element
   * @returns {string|null}
   */
  function getHighlightId(element) {
    return element.getAttribute('data-highlight-id');
  }

  // When click on mark
  document.addEventListener('click', function (event) {
    if (event.target && DOM.isElement(event.target)) {
      const ancestors = [event.target].concat(DOM.getElementAncestors(event.target));
      const markElement = ancestors.find(isMarkElement);
      if (markElement) {
        const highlightId = getHighlightId(markElement);
        if (highlightId) {
          exports.postAction({ type: 'highlightPress', id: highlightId });
        } else {
          console.warn('No highlight id found on <mark /> element');
        }
      }
    }
  });
}

function resizeIntervalService() {
  updateSize();
  setTimeout(resizeIntervalService, 1000); // every seconds
}

function resizeOnDocumentChangeService() {
  /** @type {typeof window.MutationObserver} */
  const MutationObserver =
    window.MutationObserver ||
    // @ts-ignore
    window.WebKitMutationObserver;
  const observer = new MutationObserver(updateSize);
  observer.observe(document, {
    subtree: true,
    attributes: true,
  });
}

function resizeOnWindowChangeService() {
  window.addEventListener('load', updateSize);
  window.addEventListener('resize', updateSize);
}

function clearSelection() {
  const selection = document.getSelection();
  if (selection) {
    selection.removeAllRanges();
  }
}

function handleSelectionChange() {
  const selection = document.getSelection();

  if (selection) {
    const textRange = selection.getRangeAt(0);
    const textPosition = TextPosition.fromRange(document.body, textRange);

    exports.postAction({
      type: 'selection',
      selection: {
        text: String(textRange),
        start: textPosition.start,
        end: textPosition.end,
      },
    });
  } else {
    exports.postAction({
      type: 'selection',
      selection: undefined,
    });
  }
}
exports.handleSelectionChange = handleSelectionChange;

/**
 * Main run method
 */
function main() {
  runService(consoleService, { failsafe: true });
  runService(displayService, { failsafe: true });
  runService(highlightService, { failsafe: true });
  runService(resizeIntervalService, { failsafe: true });
  runService(resizeOnDocumentChangeService, { failsafe: true });
  runService(resizeOnWindowChangeService, { failsafe: true });
  runService(actionService);

  cleanDocumentBody();
  document.addEventListener('selectionchange', handleSelectionChange);

  exports.postAction({ type: 'ready' });
}
exports.main = main;

function updateSize() {
  const documentElement = document.documentElement;
  const documentHeight = documentElement.offsetHeight;
  const documentWidth = documentElement.offsetWidth;

  const viewport = AppState.get().viewport;
  if (viewport.width !== documentWidth || viewport.height !== documentHeight) {
    AppState.update({
      viewport: {
        width: documentWidth,
        height: documentHeight,
      },
    });
    exports.postAction({ type: 'resize', documentHeight, documentWidth });
  }
}
exports.updateSize = updateSize;

/** @type {{[id: string]: HTMLElement}} */
const dom = {};

/**
 * @param {string} eventString
 * @returns {void}
 */
function handleWebViewMessage(eventString) {
  /** @type {WebViewInternalInAction} */
  const event = JSON.parse(eventString);

  switch (event.type) {
    case 'StyleUpdate': {
      const styleElement = createHeadElement(event.styleElementId, 'style');
      styleElement.innerHTML = event.css;
      break;
    }
    case 'DOMUpdate': {
      clearSelection();

      /** @type {{element: HTMLElement, childIndex?: number}[]} */
      const rootElements = [];
      event.diff.forEach((diff) => {
        try {
          switch (diff.op) {
            case 'delete_everything': {
              cleanDocumentBody();
              break;
            }
            case 'delete': {
              const element = dom[diff.id];
              if (element) {
                element.remove();
                delete dom[diff.id];
              }
              break;
            }
            case 'insert': {
              const nodeName = diff.node.name || 'span';
              /** @type {HTMLElement} */
              const element = document.createElement(nodeName);
              amendElement(element, diff.node);
              // after "amend" to override id if any:
              DOM.setAttributes(element, {
                id: diff.id,
              });
              dom[diff.id] = element;

              if (diff.node.parentId && dom[diff.node.parentId]) {
                appendElementAtIndex(element, dom[diff.node.parentId], diff.node.childIndex);
              } else {
                rootElements.push({ element, childIndex: diff.node.childIndex });
              }
              break;
            }
            case 'update': {
              const element = dom[diff.id];
              if (element) {
                amendElement(element, diff.node);
              }
              break;
            }
            default:
              break;
          }
        } catch (error) {
          console.error(String(error.stack));
        }
      });

      rootElements.forEach(({ element, childIndex }) => appendElementAtIndex(element, document.body, childIndex));
      break;
    }
    default:
    // nothing
  }
}
exports.handleWebViewMessage = handleWebViewMessage;

/**
 * @param {Record<string|number, any>} obj
 * @param {Record<string|number, any>} attribs
 * @returns {Record<string|number, any>} obj
 */
function merge(obj, attribs) {
  Object.keys(attribs).forEach((key) => {
    if (obj[key] !== null && typeof obj[key] === 'object') {
      merge(obj[key], attribs[key]);
    } else if (typeof attribs[key] === 'object') {
      obj[key] = merge(Array.isArray(attribs[key]) ? [] : {}, attribs[key]);
    } else {
      obj[key] = attribs[key];
    }
  });

  return obj;
}

/** @type {<T>(o: T, properties: Partial<T>) => T}  */
const assign =
  Object.assign ||
  function (target, properties) {
    const objectProperties = Object(properties);
    const object = Object(target);
    const keys = Object.keys(objectProperties);
    const keyCount = keys.length;
    for (let index = 0; index < keyCount; index += 1) {
      /** @type {string} */
      // @ts-ignore
      const key = keys[index];
      // eslint-disable-next-line no-prototype-builtins
      if (objectProperties.propertyIsEnumerable(key)) {
        object[key] = objectProperties[key];
      }
    }

    return object;
  };

/**
 * @returns {void}
 */
function cleanDocumentBody() {
  DOM.removeChildren(document.body);
  Object.keys(dom).forEach((id) => delete dom[id]);
}

/**
 * @param {HTMLElement} element
 * @param {NodeDiff} node
 * @returns {void}
 */
function amendElement(element, node) {
  if (node.data) {
    element.innerHTML = decodeURI(node.data);
  }
  if (node.attribs) {
    DOM.setAttributes(element, node.attribs);
  }
}

/**
 * @param {HTMLElement} element
 * @param {HTMLElement=} parent
 * @param {number=} childIndex
 * @returns {void}
 */
function appendElementAtIndex(element, parent = document.body, childIndex) {
  if (childIndex != null) {
    const refChild = parent.childNodes[childIndex];
    parent.insertBefore(element, refChild || null);
  } else {
    parent.append(element);
  }
}

/**
 *
 * @param {string} elementId
 * @param {string} tagName
 */
function createHeadElement(elementId, tagName) {
  return (
    document.getElementById(elementId) ||
    (function () {
      const element = document.createElement(tagName);
      element.id = elementId;
      document.head.append(element);

      return element;
    })()
  );
}

/**
 * @param {WebViewInternalOutAction} data
 */
function postAction(data) {
  // @ts-ignore
  ReactNativeWebView.postMessage(JSON.stringify(data));
}
exports.postAction = postAction;

/**
 *
 * @param {() => void} fn
 * @param {{ failsafe?: boolean }=} options
 */
function runService(fn, options) {
  if (options && options.failsafe) {
    // If failsafe mode we catch every error and log it as error
    try {
      fn();
    } catch (error) {
      console.error(error);
    }
  } else {
    fn();
  }
}

// start everything
// @ts-ignore
if (require.main) {
  main();
}

}).apply(this, __createContext(true));
}).call(this);
    </script>
  </body>
</html>
  `;
