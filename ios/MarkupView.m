#import <React/RCTViewManager.h>

@interface RCT_EXTERN_MODULE(MarkupViewManager, RCTViewManager)

RCT_EXPORT_VIEW_PROPERTY(markupItems, NSString)

@end
