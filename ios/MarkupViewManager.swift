//
//  MarkupViewManager.swift
//  MarkupView
//
//  Created by Lucien Boudy on 14/04/2020.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation

@objc(MarkupViewManager)
class MarkupViewManager: RCTViewManager {

    override func view() -> UIView! {
        let view = MarkupView()
        view.isEditable = false
        return view
    }

    override static func requiresMainQueueSetup() -> Bool {
        return true
    }
}

enum MarkupItemType: String {
    case text = "text"
    case media = "media"
    case line = "line"
}

protocol MarkupItem {
    var type: MarkupItemType { get }
}

struct TextItem: MarkupItem {
    let type: MarkupItemType
    let data: String
    let style: [String: String]?
}

extension TextItem {
    init(json: [String: Any]) {
        self.type = MarkupItemType.text
        self.data = json["data"] as? String ?? ""
        self.style = json["style"] as? [String: String]
    }
}

class MarkupView: UITextView {

    private var _markupItems: [MarkupItem] = [MarkupItem]()

    @objc
    var markupItems: String? {
        get {
            return combineMarkupItems()
        }
        set {
            if let json = newValue {
                _markupItems = parseMarkupJson(jsonString: json)
            }
            self.text = combineMarkupItems()
        }
    }

    func combineMarkupItems() -> String {
      return _markupItems.reduce("", { (result, item) -> String in
          switch item.type {
              case MarkupItemType.text:
                  return result + (item as! TextItem).data
              default:
                  return result
          }
      })
    }

    func parseMarkupJson(jsonString: String) -> [MarkupItem] {
        var result = [MarkupItem]()

        let data = Data(jsonString.utf8)
        let jsonData = try? JSONSerialization.jsonObject(with: data, options: [])

        if let itemArray = jsonData as? [[String: Any]] {
            for item in itemArray {
                if let itemType = item["type"] as? String {
                    switch itemType {
                    case MarkupItemType.text.rawValue:
                        result.append(TextItem(json: item))
                    default:
                        continue
                    }
                }
            }
        }
        return result
    }
}
